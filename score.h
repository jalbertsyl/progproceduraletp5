/*!
\file approximationDePi.h
\autor Jalbert Sylvain
\version 1
\date 22 octobre 2019
\brief le fichier qui contient la déclaration des fonction qui servent à approximer pi
*/

#ifndef __APPROXIMATIONDEPI_H_
#define __APPROXIMATIONDEPI_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*!
\fn int aireDisqueParQuadrillage ()
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 octobre 2019
\brief la fonction qui calcul l'air d'un disque par quadrillage, afin d'approximer pi
\return l'appriximation de pi trouvé par cette méthode
*/
float aireDisqueParQuadrillage ();

#endif
