/*!
\file score.c
\autor Jalbert Sylvain
\version 1
\date 18 octobre 2019
\brief un programme qui propose via un menu des actions et les execute si l'utilisateur les choisis
*/

#include <stdio.h>

/*!
\fn int main ( int argc, char∗∗ argv )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 23 octobre 2019
\brief la fonction principale qui affiche hello world
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return 0 si tout c'est bien passé
*/
int main (int argc, char** argv){
  //AFFICHER HELLO WORD
  printf("Hello world !\n");

  /* Fin du programme, Il se termine normalement, et donc retourne 0 */
  return 0;
}
